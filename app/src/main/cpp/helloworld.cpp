//
// Created by Soufian Madi on 07/07/2020.

//
// Created by Soufian on 07/07/2020.




#include "rodos.h"
#include <udp.h>
#include <gateway.h>
#include <sys/types.h>
#include "android_sensors.h"

extern android_app *state;

struct accData {
    float x, y, z;

    accData(float X, float Y, float Z) {
        x = X;
        y = Y;
        z = Z;

    }
};

Sensor testsensor = Sensor(ACCELEROMETER);
static CommBuffer<SensorData> accelerometerDataBuffer;
Topic<accData> accDataTopic(1010, "accelerometerTopic");
static UDPInOut udp(5959, "192.168.178.152");
static LinkinterfaceUDP interface(&udp);
static Gateway gateway(&interface, false, true);
static Subscriber subscriber(testsensor.sensorDataTopic, accelerometerDataBuffer, "accSubscriber");
SensorData buf;

class TestThread : public StaticThread<> {

    void init() {

        gateway.resetTopicsToForward();
        gateway.addTopicsToForward(&accDataTopic);

    }

    void run() {
        int loops=0;
        testsensor.setSamplingFrequency(50);
        testsensor.enable();
        testsensor.printInfo();
        suspendCallerUntil(NOW() + 100 * MILLISECONDS);
        accData data(0, 0, 0);
        TIME_LOOP(NOW(), 20 * MILLISECONDS) {
            if (accelerometerDataBuffer.getOnlyIfNewData(buf)) {
                data = accData(buf.acceleration.x,
                        buf.acceleration.y, buf.acceleration.z);
                accDataTopic.publish(data);

            }
            loops++;
            if(loops ==50){
                PRINTF("current time: %3.9f\n", SECONDS_NOW());
                loops=0;
            }

        }
    }
}threadInstance;







//////////////44444////////////////////////////////
/*
#include "rodos.h"
#include "hal/tcp.h"

TCPClient tcpClient;

class TcpClientTest : public StaticThread<> {

public:
    char strCnt[90];
    char answer[90];

    void run () {

        PRINTF(" Server shall be started first. I will wait 3 Seconds\n");
        AT(3*SECONDS);

        for(int i = 0; true ; i++) {
            suspendCallerUntil(NOW() + 500*MILLISECONDS);
            if(tcpClient.getErrorCode() != 0) {
                if(!tcpClient.reopen(5959, "192.168.178.152") ) {
                    PRINTF("no server running?\n");
                    continue;
                }
            }
            PRINTF("%s",strCnt);
            PRINTF("Client(%d)", i);
            PRINTF("sending %d '%s'\n", (int)strlen(strCnt)+1, strCnt);

            tcpClient.sendData(strCnt, static_cast<uint16_t>(strlen(strCnt)+1));
            int n = tcpClient.getData(answer, 90);
            if(n > 0) {
                PRINTF("received: %d '%s'\n", n, answer);
            } else  { PRINTF("no data\n"); }

        }
    }
} tcpClientTest;
*/

/////////////////////333///////////////////////////////
/*#include "rodos.h"


static Application senderName("UDPSender");

static UDPOut out(5959, 192,168,178,152);


class Sender : public StaticThread<> {
    char outbuf[200];

    void run () {
        int  cnt = 0;
        while(1) {
            cnt++;
            PRINTF("Sending nr %d\n", cnt);
            xsprintf(outbuf,"from UDPSender %d\n", cnt);
            out.send(outbuf, 200);
            if((cnt % 15) == 0) {
                PRINTF("Sender Wainting 3 seconds\n");
                suspendCallerUntil(NOW() + 3*SECONDS);
            }
        }
    }
};

static Sender   sender;*/
//////////////////////////////222////////////////////////////////

/*
#include "rodos.h"

static Application  receiverName("UDPReceiver");


// Assign a port ID and bind it to a socket.
// static UDPIn in(5021);



class Receiver : public StaticThread<> {

    static const uint16_t BUFFSIZE = 200;
    char userData[BUFFSIZE];      // input data buffer
    uint32_t inAddr;     // sender address IPv4
    int ip0, ip1, ip2, ip3;   // octets of sender address
    char sendHostAddress[50];  // Sender address

public:
    UDPIn in; // UDP port fo the receiver

    Receiver(const int32_t portNum) : in(portNum) { }

    void run () {
        TIME_LOOP(1*SECONDS, 20*MILLISECONDS) {
            while(int len = in.get(userData, BUFFSIZE, &inAddr)) {
                // Convert sender address to IPv4 dotted decimal notation
                ip3 = inAddr & 0xff; 	inAddr >>= 8;
                ip2 = inAddr & 0xff; 	inAddr >>= 8;
                ip1 = inAddr & 0xff; 	inAddr >>= 8;
                ip0 = inAddr & 0xff; 	inAddr >>= 8;
                xsprintf(sendHostAddress, "%d.%d.%d.%d", ip0, ip1, ip2, ip3);

                PRINTF("UDP Receiver: %d, %s; from host %s\n", len, userData, sendHostAddress);
            }
        }
    }
};


// Instantiate receiver
static Receiver receiver(5959);*/




////////////////////////111/////////////////////
/*

#include "rodos.h"
#include "hal/tcp.h"


RODOS::TCPServer tcpServer;
class TcpServerTest : public StaticThread<> {

    void run() {

        tcpServer.listen(8877);

        while (true) {

            while(!tcpServer.acceptNewConnection() ) {
                PRINTF("waiting for a new client\n");
                suspendCallerUntil(NOW() + 500*MILLISECONDS);
            }

            char  buffer[90];
            int cnt = 0;
            int nodataCnt = 0;

            while(1) {
                int len = tcpServer.getData(buffer, 90); // Warning: it seems non watind do not work for the first msg!
                if(len > 0) {
                    PRINTF("received: %d '%s'\n", len, buffer);
                    PRINTF("%s",buffer);
                    PRINTF( "Server(%d)", cnt++);
                    if(cnt % 2) tcpServer.sendData(buffer, static_cast<uint16_t>(strlen(buffer)+1));
                    nodataCnt = 0;
                }
                if(len == 0) {
                    PRINTF("waiting for data\n");
                    nodataCnt++;
                    if(nodataCnt > 10) {
                        tcpServer.acceptNewConnection();
                        PRINTF("waiting for new connection\n");
                    }
                    suspendCallerUntil(NOW() + 500*MILLISECONDS);
                }
                if(len < 0) {
                    PRINTF("connection closed\n");
                    break;
                }
            } // all data from a client
        } // loop
    }

} tcpServerTest;





*/






